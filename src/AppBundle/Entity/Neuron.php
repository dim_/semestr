<?php

namespace AppBundle\Entity;

/**
 * Neuron
 */
class Neuron
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return Neuron
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return bool
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $weights;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->weights = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add weight
     *
     * @param \AppBundle\Entity\Weight $weight
     *
     * @return Neuron
     */
    public function addWeight(\AppBundle\Entity\Weight $weight)
    {
        $this->weights[] = $weight;

        return $this;
    }

    /**
     * Remove weight
     *
     * @param \AppBundle\Entity\Weight $weight
     */
    public function removeWeight(\AppBundle\Entity\Weight $weight)
    {
        $this->weights->removeElement($weight);
    }

    /**
     * Get weights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWeights()
    {
        return $this->weights;
    }


    public function getCountRows()
    {
        return 5;
    }

    public function getCountCols()
    {
        return 8;
    }
}
