<?php

namespace AppBundle\Entity;

/**
 * MySymptom
 */
class MySymptom
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $idsStr;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MySymptom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set idsStr
     *
     * @param string $idsStr
     *
     * @return MySymptom
     */
    public function setIdsStr($idsStr)
    {
        $this->idsStr = $idsStr;

        return $this;
    }

    /**
     * Get idsStr
     *
     * @return string
     */
    public function getIdsStr()
    {
        return $this->idsStr;
    }
}

