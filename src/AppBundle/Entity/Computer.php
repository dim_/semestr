<?php

namespace AppBundle\Entity;

/**
 * Computer
 */
class Computer
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $speed;

    /**
     * @var integer
     */
    private $ram;

    /**
     * @var integer
     */
    private $hd;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var \AppBundle\Entity\Item
     */
    private $item;

    /**
     * @var \AppBundle\Entity\ComputerCd
     */
    private $computerCd;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set speed
     *
     * @param integer $speed
     *
     * @return Computer
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return integer
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set ram
     *
     * @param integer $ram
     *
     * @return Computer
     */
    public function setRam($ram)
    {
        $this->ram = $ram;

        return $this;
    }

    /**
     * Get ram
     *
     * @return integer
     */
    public function getRam()
    {
        return $this->ram;
    }

    /**
     * Set hd
     *
     * @param integer $hd
     *
     * @return Computer
     */
    public function setHd($hd)
    {
        $this->hd = $hd;

        return $this;
    }

    /**
     * Get hd
     *
     * @return integer
     */
    public function getHd()
    {
        return $this->hd;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Computer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return Computer
     */
    public function setItem(\AppBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set computerCd
     *
     * @param \AppBundle\Entity\ComputerCd $computerCd
     *
     * @return Computer
     */
    public function setComputerCd(\AppBundle\Entity\ComputerCd $computerCd = null)
    {
        $this->computerCd = $computerCd;

        return $this;
    }

    /**
     * Get computerCd
     *
     * @return \AppBundle\Entity\ComputerCd
     */
    public function getComputerCd()
    {
        return $this->computerCd;
    }
}
