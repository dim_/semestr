<?php

namespace AppBundle\Entity;

/**
 * Notebook
 */
class Notebook
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $speed;

    /**
     * @var integer
     */
    private $ram;

    /**
     * @var integer
     */
    private $hd;

    /**
     * @var integer
     */
    private $screen;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var \AppBundle\Entity\Item
     */
    private $item;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set speed
     *
     * @param integer $speed
     *
     * @return Notebook
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return integer
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set ram
     *
     * @param integer $ram
     *
     * @return Notebook
     */
    public function setRam($ram)
    {
        $this->ram = $ram;

        return $this;
    }

    /**
     * Get ram
     *
     * @return integer
     */
    public function getRam()
    {
        return $this->ram;
    }

    /**
     * Set hd
     *
     * @param integer $hd
     *
     * @return Notebook
     */
    public function setHd($hd)
    {
        $this->hd = $hd;

        return $this;
    }

    /**
     * Get hd
     *
     * @return integer
     */
    public function getHd()
    {
        return $this->hd;
    }

    /**
     * Set screen
     *
     * @param integer $screen
     *
     * @return Notebook
     */
    public function setScreen($screen)
    {
        $this->screen = $screen;

        return $this;
    }

    /**
     * Get screen
     *
     * @return integer
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Notebook
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return Notebook
     */
    public function setItem(\AppBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }
}
