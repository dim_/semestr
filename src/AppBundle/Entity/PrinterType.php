<?php

namespace AppBundle\Entity;

/**
 * PrinterType
 */
class PrinterType
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $printers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->printers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PrinterType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add printer
     *
     * @param \AppBundle\Entity\Printer $printer
     *
     * @return PrinterType
     */
    public function addPrinter(\AppBundle\Entity\Printer $printer)
    {
        $this->printers[] = $printer;

        return $this;
    }

    /**
     * Remove printer
     *
     * @param \AppBundle\Entity\Printer $printer
     */
    public function removePrinter(\AppBundle\Entity\Printer $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrinters()
    {
        return $this->printers;
    }
}
