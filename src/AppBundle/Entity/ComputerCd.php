<?php

namespace AppBundle\Entity;

/**
 * ComputerCd
 */
class ComputerCd
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $speed;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $computers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->computers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set speed
     *
     * @param string $speed
     *
     * @return ComputerCd
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return string
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Add computer
     *
     * @param \AppBundle\Entity\Computer $computer
     *
     * @return ComputerCd
     */
    public function addComputer(\AppBundle\Entity\Computer $computer)
    {
        $this->computers[] = $computer;

        return $this;
    }

    /**
     * Remove computer
     *
     * @param \AppBundle\Entity\Computer $computer
     */
    public function removeComputer(\AppBundle\Entity\Computer $computer)
    {
        $this->computers->removeElement($computer);
    }

    /**
     * Get computers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComputers()
    {
        return $this->computers;
    }
}
