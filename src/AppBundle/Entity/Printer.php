<?php

namespace AppBundle\Entity;

/**
 * Printer
 */
class Printer
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $color;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var \AppBundle\Entity\Item
     */
    private $item;

    /**
     * @var \AppBundle\Entity\PrinterType
     */
    private $printerType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set color
     *
     * @param boolean $color
     *
     * @return Printer
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return boolean
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Printer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set item
     *
     * @param \AppBundle\Entity\Item $item
     *
     * @return Printer
     */
    public function setItem(\AppBundle\Entity\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return \AppBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set printerType
     *
     * @param \AppBundle\Entity\PrinterType $printerType
     *
     * @return Printer
     */
    public function setPrinterType(\AppBundle\Entity\PrinterType $printerType = null)
    {
        $this->printerType = $printerType;

        return $this;
    }

    /**
     * Get printerType
     *
     * @return \AppBundle\Entity\PrinterType
     */
    public function getPrinterType()
    {
        return $this->printerType;
    }
}
