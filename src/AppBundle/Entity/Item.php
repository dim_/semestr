<?php

namespace AppBundle\Entity;

/**
 * Item
 */
class Item
{
   
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $maker;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $computers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notebooks;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $printers;

    /**
     * @var \AppBundle\Entity\ItemType
     */
    private $itemType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->computers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notebooks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->printers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set maker
     *
     * @param string $maker
     *
     * @return Item
     */
    public function setMaker($maker)
    {
        $this->maker = $maker;

        return $this;
    }

    /**
     * Get maker
     *
     * @return string
     */
    public function getMaker()
    {
        return $this->maker;
    }

    /**
     * Add computer
     *
     * @param \AppBundle\Entity\Computer $computer
     *
     * @return Item
     */
    public function addComputer(\AppBundle\Entity\Computer $computer)
    {
        $this->computers[] = $computer;

        return $this;
    }

    /**
     * Remove computer
     *
     * @param \AppBundle\Entity\Computer $computer
     */
    public function removeComputer(\AppBundle\Entity\Computer $computer)
    {
        $this->computers->removeElement($computer);
    }

    /**
     * Get computers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComputers()
    {
        return $this->computers;
    }

    /**
     * Add notebook
     *
     * @param \AppBundle\Entity\Notebook $notebook
     *
     * @return Item
     */
    public function addNotebook(\AppBundle\Entity\Notebook $notebook)
    {
        $this->notebooks[] = $notebook;

        return $this;
    }

    /**
     * Remove notebook
     *
     * @param \AppBundle\Entity\Notebook $notebook
     */
    public function removeNotebook(\AppBundle\Entity\Notebook $notebook)
    {
        $this->notebooks->removeElement($notebook);
    }

    /**
     * Get notebooks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotebooks()
    {
        return $this->notebooks;
    }

    /**
     * Add printer
     *
     * @param \AppBundle\Entity\Printer $printer
     *
     * @return Item
     */
    public function addPrinter(\AppBundle\Entity\Printer $printer)
    {
        $this->printers[] = $printer;

        return $this;
    }

    /**
     * Remove printer
     *
     * @param \AppBundle\Entity\Printer $printer
     */
    public function removePrinter(\AppBundle\Entity\Printer $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrinters()
    {
        return $this->printers;
    }

    /**
     * Set itemType
     *
     * @param \AppBundle\Entity\ItemType $itemType
     *
     * @return Item
     */
    public function setItemType(\AppBundle\Entity\ItemType $itemType = null)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get itemType
     *
     * @return \AppBundle\Entity\ItemType
     */
    public function getItemType()
    {
        return $this->itemType;
    }
}
