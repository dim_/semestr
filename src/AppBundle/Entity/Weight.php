<?php

namespace AppBundle\Entity;

/**
 * Weight
 */
class Weight
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $inputIndex;

    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inputIndex
     *
     * @param integer $inputIndex
     *
     * @return Weight
     */
    public function setInputIndex($inputIndex)
    {
        $this->inputIndex = $inputIndex;

        return $this;
    }

    /**
     * Get inputIndex
     *
     * @return int
     */
    public function getInputIndex()
    {
        return $this->inputIndex;
    }

    
    /**
     * @var \AppBundle\Entity\Neuron
     */
    private $neuron;


    /**
     * Set neuron
     *
     * @param \AppBundle\Entity\Neuron $neuron
     *
     * @return Weight
     */
    public function setNeuron(\AppBundle\Entity\Neuron $neuron = null)
    {
        $this->neuron = $neuron;

        return $this;
    }

    /**
     * Get neuron
     *
     * @return \AppBundle\Entity\Neuron
     */
    public function getNeuron()
    {
        return $this->neuron;
    }
    
    

    /**
     * @var float
     */
    private $value;


    /**
     * Set value
     *
     * @param float $value
     *
     * @return Weight
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
}
