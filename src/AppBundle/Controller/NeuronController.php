<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Neuron;
use AppBundle\Entity\Weight;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NeuronController extends Controller
{

    /**
     * @Route("/neuron/work", name="neuron_work")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function workAction(Request $request)
    {
        $answers = $this->getAnswers($request);

        $latin = ($answers['first'] > $answers['second']);

        return $this->render('default/neuron_answer.html.twig', ['latin' => $latin]);
    }

    /**
     * @Route("/neuron/learn", name="neuron_learn")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function learnAction(Request $request)
    {
        $first = $this->neuronRepo()->findOneBy(['type' => true]); //latin
        $second = $this->neuronRepo()->findOneBy(['type' => false]);
        $answers = $this->getAnswers($request);
        $answer1 = $answers['first'];
        $answer2 = $answers['second'];
        $reference1 = ($request->get('type') == 1)?1:0;
        $reference2 = ($request->get('type') == 2)?1:0;

        for($i = 0; $i < ($first->getCountCols() * $first->getCountRows()); ++$i)
        {
            $weight = $this->weightRepo()->findOneBy([
                'neuron' => $first,
                'inputIndex' => $i
            ]);

            var_dump('<br>weight #'.$weight->getId().' was '.$weight->getValue());

            $weight->setValue(
                $weight->getValue() + $this->getDelta($request->get('input_'.$i), $answer1, $reference1)
            );

            var_dump('<br>weight #'.$weight->getId().' became '.$weight->getValue());

            $this->plush($weight);

            $weight = $this->weightRepo()->findOneBy([
                'neuron' => $second,
                'inputIndex' => $i
            ]);

            var_dump('<br>weight #'.$weight->getId().' was '.$weight->getValue());

            $weight->setValue(
                $weight->getValue() + $this->getDelta($request->get('input_'.$i), $answer2, $reference2)
            );

            var_dump('<br>weight #'.$weight->getId().' became '.$weight->getValue());

            $this->plush($weight);
        }

        return $this->learnPage();
    }
    
    public function getDelta($input, $answer, $reference)
    {
        return $input * 0.5 * ($reference - $answer);
    }
    
    public function getAnswers(Request $request)
    {
        $first = $this->neuronRepo()->findOneBy(['type' => true]); //latin
        $second = $this->neuronRepo()->findOneBy(['type' => false]);
        $sum1 = 0;
        $sum2 = 0;

        for($i = 0; $i < ($first->getCountCols() * $first->getCountRows()); ++$i)
        {
            $weight = $this->weightRepo()->findOneBy([
                'neuron' => $first,
                'inputIndex' => $i
            ]);

            $sum1 += $weight->getValue() * $request->get('input_'.$i);

            $weight = $this->weightRepo()->findOneBy([
                'neuron' => $second,
                'inputIndex' => $i
            ]);

            $sum2 += $weight->getValue() * $request->get('input_'.$i);
        }

        $answer1 = 1 / (1 + exp(-1 * $sum1));
        $answer2 = 1 / (1 + exp(-1 * $sum2));
        
        return ['first' => $answer1, 'second' => $answer2];
    }

    public function createNetwork()
    {
        $neurons = $this->neuronRepo()->findAll();

        if (count($neurons) == 0)
        {
            $first = new Neuron();
            $first->setType(true);
            $this->plush($first);
            $second = new Neuron();
            $second->setType(false);
            $this->plush($second);

            for($i = 0; $i < ($first->getCountCols() * $first->getCountRows()); ++$i)
            {
                $weight = new Weight();
                $weight->setNeuron($first);
                $weight->setInputIndex($i);
                $weight->setValue(random_int(-10, 10));
                $this->plush($weight);
                $weight = new Weight();
                $weight->setNeuron($second);
                $weight->setInputIndex($i);
                $weight->setValue(random_int(-10, 10));
                $this->plush($weight);
            }
        }

    }

    /**
     * @Route("/neuron/work_page", name="neuron_work_page")
     */
    public function workPage()
    {
        $this->createNetwork();

        return $this->render('default/neuron_work_form.html.twig', ['neuron' => new Neuron()]);
    }

    /**
     * @Route("/neuron/learn_page", name="neuron_learn_page")
     */
    public function learnPage()
    {
        $this->createNetwork();

        return $this->render('default/neuron_learn_form.html.twig', ['neuron' => new Neuron()]);
    }

    public function weightRepo()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Weight');
    }

    public function neuronRepo()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Neuron');
    }
    
    public function dd($value)
    {
        die(var_dump($value));
    }

    public function plush($obj)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($obj);
        $em->flush();
    }
    
    public function delete($obj)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($obj);
        $em->flush();
    }

    /**
     * @Route("/neuron/hopfield", name="neuron_hopfield")
     */
    public function hopfield(){
        set_time_limit(0);

        $d = [-1, 1, 1, 1, -1, -1, 1, -1, 1, -1, -1, 1, -1, 1, -1, -1, 1, -1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1];
        $i = [1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, 1, 1, 1, -1, 1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1];
        $m = [1, -1, -1, -1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, 1, -1, 1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1];
        $a = [-1, -1, 1, -1, -1, -1, 1, -1, 1, -1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1, 1, -1, -1, -1, 1];

        $x = [$d, $i, $m, $a];

        $w = [];

        for($i = 0; $i < count($d); ++$i )
        {
            for($j = 0; $j < count($d); ++$j )
            {
                $w[$i][$j] = 0;
            }
        }

        foreach ($x as $arr)
        {
            for($i = 0; $i < count($d); ++$i )
            {
                for($j = 0; $j < count($d); ++$j )
                {
                    if($i != $j)
                    {
                        $w[$i][$j] += $arr[$i]*$arr[$j];
                    }
                }
            }
        }



        $y1 = [];
        $wrong_d = [1, 1, 1, 1, -1, -1, 1, -1, 1, -1, -1, -1, 1, 1, -1, -1, 1, 1, 1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 1, 1, 1, -1, 1];


        for($i = 0; $i < count($d); ++$i )
        {
            for($j = 0; $j < count($d); ++$j )
            {
                echo  $w[$i][$j] . ' ';
            }
            echo '<br>';
        }


        for($j = 0; $j < count($d); ++$j )
        {
            $y1[$j] = 0;
        }

        for($i = 0; $i < count($d); ++$i )
        {
            for($j = 0; $j < count($d); ++$j )
            {
                $y1[$i] += $w[$i][$j] * $wrong_d[$j];
            }

            if($y1[$i] >= 0){
                $y1[$i] = 1;
            }else{
                $y1[$i] = -1;
            }
        }

        echo '<br>';
        echo '<br>';
        echo '<br>';
        for($j = 0; $j < count($d); ++$j )
        {
            echo  $d[$j] . ' ';
        }
        echo '<br>';

        echo '<br>';
        echo '<br>';
        echo '<br>';
        for($j = 0; $j < count($d); ++$j )
        {
            echo  $y1[$j] . ' ';
        }
        echo '<br>';


        $y_next = [];

        for($j = 0; $j < count($d); ++$j )
        {
            $y_next[$j] = 0;
        }

        $y_prev = $y1;

        $flag = true;
        $count = 0;

        while ($flag)
        {
            $count++;


            for($i = 0; $i < count($d); ++$i )
            {
                for($j = 0; $j < count($d); ++$j )
                {
                    $y_next[$i] += $w[$i][$j] * $y_prev[$j];
                }

                if($y_next[$i] >= 0){
                    $y_next[$i] = 1;
                }else{
                    $y_next[$i] = -1;
                }
            }

            if($y_prev === $y_next)
            {
                $flag = false;
            }else{
                $y_prev = $y_next;
            }
        }

        $this->dd($y_next === $d);

    }
}
