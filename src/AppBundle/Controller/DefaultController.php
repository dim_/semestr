<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MyObject;
use AppBundle\Entity\MySymptom;
use AppBundle\Entity\Tweet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/twitter/test", name="twitter_test")
     */
    public function testAction()
    {
        $twitter_client = new \Guzzle\Http\Client('https://api.twitter.com/{version}', array(
            'version' => '1.1'
        ));

        $twitter_client->addSubscriber(new \Guzzle\Plugin\Oauth\OauthPlugin(array(
            'consumer_key'  => 'rHJPsGKiGY6jRWa8G8ZKX1Ovk',
            'consumer_secret' => 'kr1e5fAecet32oAZjCzyWAwevKXod1CAESq9CsU8eopYXVRk1l',
            'token'       => '938861369683738625-FBZIvMlVyD0bwTWBsQMvoJuFhtPaiz7',
            'token_secret'  => 'lPurJjDXMsK88uJt5ZNaK6NkMZcFcELb1zAZt6N5q4Eiq'
        )));

        $request = $twitter_client->get('statuses/user_timeline.json');
        $request->getQuery()->set('screen_name', 'elonmusk')/*->set('max_id', 936782477502246912)*/;
        $response = $request->send();

        $tweets = json_decode($response->getBody());



        $this->dd($tweets);
    }

    /**
     * @Route("/twitter/account/{user}", name="twitter_get_tweets")
     */
    public function tweetsAction($user)
    {
        $twitter_client = new \Guzzle\Http\Client('https://api.twitter.com/{version}', array(
            'version' => '1.1'
        ));

        $twitter_client->addSubscriber(new \Guzzle\Plugin\Oauth\OauthPlugin(array(
            'consumer_key'  => 'rHJPsGKiGY6jRWa8G8ZKX1Ovk',
            'consumer_secret' => 'kr1e5fAecet32oAZjCzyWAwevKXod1CAESq9CsU8eopYXVRk1l',
            'token'       => '938861369683738625-FBZIvMlVyD0bwTWBsQMvoJuFhtPaiz7',
            'token_secret'  => 'lPurJjDXMsK88uJt5ZNaK6NkMZcFcELb1zAZt6N5q4Eiq'
        )));

        $request = $twitter_client->get('statuses/user_timeline.json');
        $request->getQuery()->set('screen_name', $user);

        $count = $this->getDoctrine()->getRepository('AppBundle:Tweet')->getCountTweets($user);

        $maxId = '-777';

        if($count > 0){
            $maxId =  $this->getDoctrine()->getRepository('AppBundle:Tweet')->getMinId($user);

            if($maxId){
                $request->getQuery()->set('max_id', $maxId);
            }
        }

        $response = $request->send();

        $tweets = json_decode($response->getBody());

        foreach ($tweets as $tweet){

            if($tweet->id_str != $maxId) {
                $obj = new Tweet();
                $obj->setCreatedAt($tweet->created_at);
                $obj->setTwitterId($tweet->id_str);
                $obj->setText($tweet->text);
                $obj->setUserId($tweet->user->id_str);
                $obj->setUserName($tweet->user->name);
                $obj->setScreenName($tweet->user->screen_name);
                $obj->setLocation($tweet->user->location);
                $obj->setDescription($tweet->user->description);
                $obj->setVerified($tweet->user->verified);
                $obj->setProfileImage($tweet->user->profile_image_url);
                $obj->setFavoriteCount($tweet->favorite_count);
                $obj->setRetweetCount($tweet->retweet_count);
                $obj->setLang($tweet->lang);
                $obj->setSource($tweet->source);
                $this->plush($obj);
            }

        }

        return $this->render('default/tweets.html.twig', [
            'objects' => $this->getDoctrine()->getRepository('AppBundle:Tweet')->findAll()
        ]);

    }
    
    
    /**
     * @Route("/", name="home_page")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/add/form", name="add_form")
     */
    public function addForm()
    {
        return $this->render('default/add_form.html.twig');
    }

    /**
     * @Route("/add/action", name="add_action")
     * @param Request $request
     * @return RedirectResponse
     */
    public function addAction(Request $request)
    {
        $obj = new MyObject();
        $obj->setName($request->get('name'));
        $this->plush($obj);

        return $this->redirectToRoute('list');
    }

    /**
     * @Route("/list", name="list")
     */
    public function listAction()
    {
        $objects = $this->obj_repo()->findAll();
        $symptoms = $this->symptoms_repo()->findAll();
        
        return $this->render('default/list.html.twig',['objects' => $objects, 'symptoms' => $symptoms]);
    }

    /**
     * @Route("/add/symptom/{obj_id}", name="add_symptom")
     * @param Request $request
     * @param $obj_id
     * @return RedirectResponse
     */
    public function addSymptomAction(Request $request, $obj_id)
    {
        $obj = $this->obj_repo()->find($obj_id);
        
        if(!$obj instanceof MyObject)
        {
            return $this->redirectToRoute('list');
        }
        
        $ids_part = ((string)$obj_id).'#';
        $name = $request->get('name');
        $repo = $this->symptoms_repo();
        $symptom = $repo->findOneBy([
            'name' => $name
        ]);

        if($symptom instanceof MySymptom)
        {
            $str = $symptom->getIdsStr();
            $pos = strpos($str, $ids_part);

            if ($pos === false) 
            {
                $str .= $ids_part;
                $symptom->setIdsStr($str);
            }
        }
        else
        {
            $symptom = new MySymptom();
            $symptom->setName($name);
            $symptom->setIdsStr($ids_part);
        }
        
        $this->plush($symptom);

        return $this->redirectToRoute('list');
    }

    /**
     * @Route("/delete/obj/{id}", name="delete_obj")
     * @param $id
     * @return RedirectResponse
     */
    public function delete_object($id)
    {
        $obj = $this->obj_repo()->find($id);

        if(!$obj instanceof MyObject)
        {
            return $this->redirectToRoute('list');
        }

        /** @var MySymptom[] $symptoms */
        $symptoms = $this->symptoms_repo()->getByObjId($id);

        foreach ($symptoms as $symptom)
        {
            $this->delete_symptom_by_obj_id($symptom,$id);
        }

        $this->delete($obj);

        return $this->redirectToRoute('list');
    }

    /**
     * @Route("/symptoms/obj/{id}", name="symptoms")
     * @param $id
     * @return RedirectResponse
     */
    public function symptoms_list($id)
    {
        $obj = $this->obj_repo()->find($id);

        if(!$obj instanceof MyObject)
        {
            return $this->redirectToRoute('list');
        }

        /** @var MySymptom[] $symptoms */
        $symptoms = $this->symptoms_repo()->getByObjId($id);

        return $this->render('default/symptoms.html.twig', ['symptoms' => $symptoms, 'obj_id' => $id]);
    }

    /**
     * @Route("/delete/symptom/{id}/{obj_id}", name="delete_symptom")
     * @param $id
     * @param $obj_id
     * @return RedirectResponse
     */
    public function delete_symptom($id, $obj_id)
    {
        $symptom = $this->symptoms_repo()->find($id);
        $obj = $this->obj_repo()->find($obj_id);
        
        if($symptom instanceof MySymptom && $obj instanceof MyObject)
        {
            $this->delete_symptom_by_obj_id($symptom, $obj_id);
        }

        return $this->redirectToRoute('symptoms',['id'=>$obj_id]);
    }

    /**
     * @Route("/expert/form", name="expert_form")
     */
    public  function expertForm()
    {
        $symptoms = $this->symptoms_repo()->getSymptoms();
        return $this->render('default/expert_from.html.twig', ['symptoms' => $symptoms]);
    }

    /**
     * @Route("/expert/system", name="expert_system")
     * @param Request $request
     * @return Response
     */
    public function expertSystem(Request $request)
    {
        $symptoms_ids = $request->get('checked');
        $answers_ids = [];
        $first = true;

        foreach ($symptoms_ids as $symptom_id)
        {
            $symptom = $this->symptoms_repo()->find($symptom_id);
            $objects = $this->from_str_to_array($symptom->getIdsStr());

            if(count($answers_ids) == 0 && $first)
            {
                $answers_ids = $objects;
                $first = false;
            }
            else
            {
                if(!count($answers_ids))
                {
                    break;
                }

                $to_delete = [];
                foreach ($answers_ids as  $answer_id)
                {
                    $same = false;
                    foreach ($objects as $object_id)
                    {
                        if ($answer_id == $object_id)
                        {
                            $same = true;
                            break;
                        }
                    }

                    if(!$same)
                    {
                        $to_delete[] = $answer_id;
                    }
                }
//                echo 'want to delete: ';
//                var_dump($to_delete);
//                echo '<br>now: ';
//                var_dump($answers_ids);
                $answers_ids = array_diff($answers_ids, $to_delete);
//                echo '<br>after: ';
//                var_dump($answers_ids);
//                echo '<br>';
            }
        }
//        echo '<br>finish : ';
//        var_dump($answers_ids);
//        echo '<br>';
        $objects = [];
        foreach ($answers_ids as $answer_id)
        {
            $object = $this->obj_repo()->find($answer_id);

            if($object instanceof MyObject)
            {
                $objects[] = $object;
            }
        }

        return $this->render('default/answer.html.twig', ['objects' => $objects]);
    }

    public function from_str_to_array($str)
    {
        $str = str_split($str);
        $arr = [];
        $temp = '';

        foreach ($str as $c)
        {
            if($c == '#')
            {
                $arr[] = (int)$temp;
                $temp = '';
            }
            else
            {
                $temp .= $c;
            }
        }

        return $arr;
    }
    
    
    public function delete_symptom_by_obj_id(MySymptom $symptom, $id)
    {
        $new_str = $this->delete_id_from_str($symptom->getIdsStr(),$id);

        if($new_str == '')
        {
            $this->delete($symptom);
        }
        else
        {
            $symptom->setIdsStr($new_str);
            $this->plush($symptom);
        }
    }

    public function delete_id_from_str($str, $id)
    {
        $ids_part = ((string)$id).'#';
        $pos = strpos($str, $ids_part);

        if ($pos === false)
        {
            return $str;
        }
        else
        {
            $str = str_split($str);
            $ids_part = str_split($ids_part);
            $new_str = '';

            for($i = 0; $i < $pos; ++$i)
            {
                $new_str .= $str[$i];
            }

            for($i = ($pos + count($ids_part)); $i < count($str); ++$i )
            {
                $new_str .= $str[$i];
            }

            return $new_str;
        }
    }
    
    public function obj_repo()
    {
        return $this->getDoctrine()->getRepository('AppBundle:MyObject');
    }

    public function symptoms_repo()
    {
        return $this->getDoctrine()->getRepository('AppBundle:MySymptom');
    }
    
    public function dd($value)
    {
        die(var_dump($value));
    }

    public function plush($obj)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($obj);
        $em->flush();
    }
    
    public function delete($obj)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($obj);
        $em->flush();
    }
}
