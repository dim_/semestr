<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MitSevenController extends Controller
{
    /**
     * @Route("/makers", name="makers")
     */
    public function indexAction()
    {
        $makers = $this->getDoctrine()->getRepository('AppBundle:Item')->getMakers();

        return $this->render('default/makers.html.twig', ['makers' => $makers]);
    }

    /**
     * @Route("/maker/{name}", name="products")
     */
    public function pruductsAction($name)
    {
        
        $item_ids = $this->getDoctrine()->getRepository('AppBundle:Item')->getIdsByMaker($name);
        $ids = [];

        foreach ($item_ids as $items) {
            foreach ($items as $key => $value) {
                $ids[] = $value;
            }
        }

        

        $computers = $this->getDoctrine()->getRepository('AppBundle:Computer')->getByItemIds($ids);
        $notebooks = $this->getDoctrine()->getRepository('AppBundle:Notebook')->getByItemIds($ids);
        $printers = $this->getDoctrine()->getRepository('AppBundle:Printer')->getByItemIds($ids);

        return $this->render('default/products.html.twig', ['computers' => $computers, 'notebooks' => $notebooks,
         'printers' => $printers]);
    }

    
}
